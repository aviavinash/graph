import { Component, OnInit } from '@angular/core';
declare var require;
declare var $: any;
declare var jquery: any;
const cytoscape = require('cytoscape');




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  nodes = [
    {
      id: 'a', position: { x: 300, y: 400 }, targets: [{ id: 'ab', connect_to: 'b' }],
      tooltip_data: { 'header': 'Headinga', body: 'data a', editable: 'ta' }
    },
    {
      id: 'b', position: { x: 531, y: 116 }, targets: [],
      tooltip_data: { 'header': 'Headingb', body: 'data b', editable: 'ta' }
    },
    {
      id: 'c', position: { x: 200, y: 400 }, targets: [{ id: 'bc', connect_to: 'a' }],
      tooltip_data: { 'header': 'Headingc', body: 'data c', editable: 'ta' }
    }
  ];

  cy: any;


  constructor() { }

  generateGraph() {
    this.nodes.forEach((node) => {
      this.cy.add({
        data: {
          id: node.id
        },
        position: node.position
      });

    });

    this.nodes.forEach((node) => {
      if (node.targets && node.targets.length !== 0) {
        node.targets.forEach(target => {
          this.cy.add({
            id: target.id,
            data: {
              position: { // the model position of the node (optional on init, mandatory after)
                x: 200,
                y: -200
              },
              source: node.id,
              target: target.connect_to
            }
          });

        });
      }
    });
    const cy1 = this.cy;
    const nodes = this.nodes;
    cy1.on('mouseover', 'node', function (evt) {
      const pos = cy1.$('#' + this.id()).position();
      if (this.id()) {
        nodes.forEach((node) => {
          if (node.id === this.id()) {
            document.getElementById('showdiv').style.left = evt.originalEvent.x + 'px';
            document.getElementById('showdiv').style.top = (evt.originalEvent.y) + 'px';
            document.getElementById('showdiv').style.display = 'block';
            document.getElementById('showdiv').className += ' class_two';
            document.getElementById('header').innerHTML = node.tooltip_data.header;
            document.getElementById('content').innerHTML = node.tooltip_data.body;
            document.getElementById('cy').appendChild(document.getElementById('showdiv'));
          }
        });
      }
    });
    cy1.on('mouseout', 'node', function (evt) {
      document.getElementById('showdiv').style.display = 'none';
    });

    // cy1.on('tap', 'node', function (evt) {
    //   console.log('node targert', evt);
    //   const node = evt.cyTarget;
    //   console.log(evt.id());
    // });

    cy1.on('tap', 'node', (evt) => {
      console.log(evt);
      evt.cyTarget.connectedEdges().animate({
        style: {lineColor: "red"}
      });
      console.log('cyTarget.connectedEdges()', evt.cyTarget.connectedEdges());
     });

    cy1.on('tap', 'node', function (evt) {
      console.log('get edge id', evt.cyTarget.id())
    });
    let edgeid;
    cy1.on('tap', 'edge', function (evt) {
    edgeid = evt.cyTarget[0].data().source + evt.cyTarget[0].data().target;
      console.log('edgeid', edgeid);

      // if (event.keyCode == 46){
      //   console.log('Delete Key Pressed');
      // }
      // console.log(event);
      // console.log(event.keyCode);

    });
    cy1.on('keypress', 'edge', function(evt) {
      console.log(evt);
      console.log(edgeid);
    });


     cy1.on('select', 'node', function(evt) {
       console.log('selct evt', evt);
      evt.cyTarget.connectedEdges().animate({
      style: { lineColor: 'blue' }
      });
      evt.cyTarget.nodes().animate({
        style: {'background-color': 'yellow'}
    });
    });

    cy1.on('click', 'node', function(evt) {
      evt.cyTarget.connectedEdges();
      console.log('evt target edge', evt.cyTarget.connectedEdges());
    });



    cy1.on('drag', 'node', function (evt) {
      document.getElementById('showdiv').style.display = 'none';
      const pos = cy1.$('#' + this.id()).position();
      nodes.forEach((node) => {
        if (node.id === this.id()) {
          node.position.x = pos.x;
          node.position.y = pos.y;
        }
      });
      console.log(nodes)
    });
  }

  ngOnInit() {
    document.getElementById('showdiv').style.display = 'none';
    this.cy = cytoscape({
      container: document.getElementById('cy'),
    });
    this.generateGraph();
  }
}
