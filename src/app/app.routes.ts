import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/pages/home/home.component';


export const AppRoutes: Routes = [
{ path: '', redirectTo: 'home', pathMatch: 'full' },
{ path: 'home',  component: HomeComponent },

];

export const AppRouteRoot = RouterModule.forRoot(AppRoutes, { useHash: true });